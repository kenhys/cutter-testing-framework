<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE refentry 
  PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
  "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<refentry id="cut-diff">
<refmeta>
  <refentrytitle role='top_of_page' id='cut-diff.top_of_page'>cut-diff</refentrytitle>
  <refmiscinfo>Cutterライブラリ</refmiscinfo>
</refmeta>
<refnamediv>
  <refname>cut-diff</refname>
  <refpurpose>色付きで2つのファイルの違いを表示</refpurpose>
</refnamediv>
<refsect1>
  <title>名前</title>
  <para>cut-diff - 色付きで2つのファイルの違いを表示</para>
</refsect1>

<refsect1>
  <title>書式</title>
  <para>
  <code>cut-diff</code>
   [
  <emphasis>オプション...</emphasis>
  ] 
  <emphasis>ファイル1</emphasis>
   
  <emphasis>ファイル2</emphasis>
  
</para>
</refsect1>

<refsect1>
  <title>説明</title>
  <para>cut-diffはCutterで使用している差分表示機能を利用したdiffコマ ンドです。色付きでわかりやすく差分を表示したい場合に便利です。 patchコマンドと一緒に使う、色がなくても問題ない、などの場合 は通常のdiffコマンドがおすすめです。</para>
</refsect1>

<refsect1>
  <title>オプション</title>
  <variablelist>
  <varlistentry>
  <term id='cut-diff.--version'>--version</term>
  <listitem>
  <para>バージョンを表示して終了します。</para>
</listitem>
</varlistentry>
  <varlistentry>
  <term id='cut-diff.-c'>-c [yes|true|no|false|auto], --color=[yes|true|no|false|auto]</term>
  <listitem>
  <para>
  色付きで差分を表示するかどうかを指定します。
  <code>yes</code>
  または
  <code>true</code>
  の場合は色付きで表示します。
  <code>no</code>
  または
  <code>false</code>
  の場合は色なしで表示します。
  <code>auto</code>
  の場合は色付けできそうな場合は色付きで表示し、 そうでない場合は色なしで表示します。
</para>
  <para>デフォルトはautoです。</para>
</listitem>
</varlistentry>
  <varlistentry>
  <term id='cut-diff.-u'>-u, --unified</term>
  <listitem>
  <para>unified diff形式で出力します。</para>
</listitem>
</varlistentry>
  <varlistentry>
  <term id='cut-diff.--context-lines'>--context-lines=LINES</term>
  <listitem>
  <para>
  差分の周辺
  <code>LINES</code>
  行を表示します。
</para>
  <para>デフォルトでは全部の行を表示します。unified diff形式の場 合は3行です。</para>
</listitem>
</varlistentry>
  <varlistentry>
  <term id='cut-diff.--label'>--label=LABEL, -L=LABEL</term>
  <listitem>
  <para>
  ヘッダのラベルに
  <code>LABEL</code>
  を使います。1つめの
  <emphasis>--label</emphasis>
  オプションで指定した値は
  <emphasis>file1</emphasis>
  のラベル になり、2つめの
  <emphasis>--label</emphasis>
  オプションで指定した値は
  <emphasis>file2</emphasis>
  のラベルになります。
</para>
  <para>デフォルトではファイル名を使います。</para>
</listitem>
</varlistentry>
</variablelist>
</refsect1>

<refsect1>
  <title>終了ステータス</title>
  <para>エラーが発生した場合は0以外、そうでない場合は0を返します。</para>
  <para>TODO: 変更がない場合は0、変更があった場合は1、エラーが起きた 場合は2を返すこと。</para>
</refsect1>

<refsect1>
  <title>例</title>
  <para>
  以下の例は、
  <code>file1</code>
  と
  <code>file2</code>
  の差分を表示します。
</para>
  <programlisting>% cut-diff file1 file2</programlisting>
  <para>
  以下の例は、
  <code>file1</code>
  と
  <code>file2</code>
  の差分をunified diff形式 で表示します。
</para>
  <programlisting>% cut-diff -u file1 file2</programlisting>
</refsect1>

<refsect1>
  <title>関連項目</title>
  <para>diff(1)</para>
</refsect1>
</refentry>
