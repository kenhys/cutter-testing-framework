.\" DO NOT MODIFY THIS FILE! it was generated by rd2
.TH CUT\-DIFF 1 "September 2019" "Cutter" "Cutter's manual"
.SH NAME
.PP
cut\-diff \- show difference between 2 files with color
.SH SYNOPSIS
.PP
\&\fBcut\-diff\fP [\fIoption ...\fP] \fIfile1\fP \fIfile2\fP
.SH DESCRIPTION
.PP
cut\-diff is a diff command that uses diff feature in Cutter. It shows difference with color.
.PP
It's recommended that you use a normal diff(1) when you want to use with patch(1) or you don't need color.
.SH OPTIONS
.TP
.fi
.B
\-\-version
cut\-diff shows its own version and exits.
.TP
.fi
.B
\-c [yes|true|no|false|auto], \-\-color=[yes|true|no|false|auto]
If 'yes' or 'true' is specified, cut\-diff uses colorized output by escape sequence. If 'no' or 'false' is specified, cut\-diff never use colorized output. If 'auto' or the option is omitted, cut\-diff uses colorized output if available.

The default is auto.
.TP
.fi
.B
\-u, \-\-unified
cut\-diff uses unified diff format.
.TP
.fi
.B
\-\-context\-lines=LINES
Shows diff context around \&\fBLINES\fP.

All lines are shown by default. When unified diff format is used, 3 lines are shown by default.
.TP
.fi
.B
\-\-label=LABEL, \-L=LABEL
Uses \&\fBLABEL\fP as a header label. The first\fI\-\-label\fP option value is used as \fIfile1\fP's label and the second \fI\-\-label\fP option value is used as\fIfile2\fP's label.

Labels are the same as file names by default.
.SH EXIT STATUS
.PP
The exit status is 0 for success, non\-0 otherwise.
.PP
TODO: 0 for non\-difference, 1 for difference and non\-0 for errors.
.SH EXAMPLE
.PP
In the following example, cut\-diff shows difference between \&\fBfile1\fP and \&\fBfile2\fP:
.nf
\&    % cut\-diff file1 file2
.fi
.PP
In the following example, cut\-diff shows difference between \&\fBfile1\fP and \&\fBfile2\fP with unified diff format:
.nf
\&    % cut\-diff \-u file1 file2
.fi
.SH SEE ALSO
.PP
diff(1)

