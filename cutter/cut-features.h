/* cut-features.h
 *
 * This is a generated file.  Please modify 'configure.ac'
 */

#ifndef __CUT_FEATURES_H__
#define __CUT_FEATURES_H__

/**
 * SECTION: cut-features
 * @title: Available features
 * @short_description: Available features in the installed Cutter.
 *
 * There are some macros to check a feature is available in the
 * installed Cutter.
 */

/**
 * CUT_SUPPORT_GLIB:
 *
 * Shows GLib support is available. That is, we can use &lt;gcutter.h&gt;.
 * It is always defined.
 */
#define CUT_SUPPORT_GLIB 1

/**
 * CUT_SUPPORT_GDK_PIXBUF:
 *
 * Shows GdkPixbuf support is available.
 * That is, we can use &lt;gdkcutter-pixbuf.h&gt;.
 */
#define CUT_SUPPORT_GDK_PIXBUF 1

/**
 * CUT_SUPPORT_GIO:
 *
 * Shows GIO support is available.
 */
#define CUT_SUPPORT_GIO 1

/**
 * CUT_SUPPORT_LIBSOUP:
 *
 * Shows LibSoup support is available.
 */
#define CUT_SUPPORT_LIBSOUP 1

/**
 * CUT_SUPPORT_C99_STDINT_TYPES:
 *
 * Shows C99 stdint types support is available.
 * That is, we can use assertions that use C99 stdint types.
 * e.g. cut_assert_equal_int_least8().
 */
#define CUT_SUPPORT_C99_STDINT_TYPES 1

#endif
